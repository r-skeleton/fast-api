# FAST API SKELETON

## Configurations

- This project settings are configured as classes. To init configs from `.env` files with prefix, include the prefix in
  inside class `Config` and defines variables without prefix. You probably need to assign default values and also its
  data types.
- Configure API Metadata inside `api_metadata` in `APISettings`
- Example:
  ```python
  from pydantic import BaseSettings
  from typing import Optional
  from functools import lru_cache
  
  class CelerySettings(BaseSettings):
       queues: set = (

       )
       broker_url: str
       result_backend: Optional[str] = None
       accept_content: list = ['json']
       task_serializer: str = 'json'
       result_serializer: str = 'json'

      # Include the prefix to search every variable inside the `.env` file
      class Config:
        env_prefix = 'CELERY_'

  @lru_cache()
  def get_celery_setting() -> CelerySettings:
    return CelerySettings()

  ```

## Coding

### `views.py` setups and coding.

- Declare an APIRouter
- To specify the tags metadata on docs site:
    + structures:
      ```python
      tags_metadata = {
          "name": "items",
          "description": "Manage items. So _fancy_ they have their own docs.",
          "externalDocs": {
              "description": "Items external docs",
              "url": "https://fastapi.tiangolo.com/",
          }
      }
      ```
    + exports to app - export with views Router:
      ```python
      # modules __init__.py file.
      from .views import test_v1
      from .views import tags_metadata as test_v1_tags
      
      # app.py
      tags_metadata = [
        # include tags in here.
        test_v1_tags,
      ]

      ```
- To include descriptions of an API:
  It supports markdown like *.md files.
  ```python
  async def post_test():
      # Specify the description to show on docs. 
      """
      Create an item with all the information:

        - **name**: each item must have a name
        - **description**: a long description
        - **price**: required
        - **tax**: if the item doesn't have tax, you can omit this
        - **tags**: a set of unique tag strings for this item
      """
  ```
- You may also want to include the schema as a `response_model`.
  ```python
  from .models import Test
  
  @test_v1.post('/test', response_model=Test)
  async def test():
      pass
  ```

### `models` module:

- The `Test` model should look like:
  ```python
  from typing import Optional
  from pydantic import BaseModel


  class Test(BaseModel):
      name: str
      description: Optional[str] = None
  ```
- __*Note*__:
    - The `Test` model should be created inside the `models` module and export in `models/__init__.py`
    - This model can be used as a Response model or Validation schema

## API Documents

- Web view:
    + swagger docs: `host:port/docs`. Example: `localhost:8000/docs`
    + swagger redoc: `host:port/redoc`. Example: `localhost:8000/redoc`
- General Descriptions and Responses: These should be included in `APISettings`, `responses` field
- API Specific Descriptions and Responses: Can easily declare at `views.py` of each module.
  + Example:
    ```python
    from fastapi import APIRouter
    from .view_model import TestModel
    from .models import Test
    
    
    # Declare Responses in here
    responses = {
     499: {
       'description': 'Error'
      }
    }

    test_v1 = APIRouter(prefix='/v1/test', tags=['test'], responses=responses)
    ```

## Deployment

```shell
# Step 1:
# Create an .env file
$ touch .env

# or copy from .template.env
$ cp .template.env .env

# Step 2:
# General command
$ docker-compose up --build

# Run silently
$ docker-compose up --build -d
```