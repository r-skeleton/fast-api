from fastapi import HTTPException


class BaseServerException(HTTPException):
    status_code = None

    def __init__(self, detail):
        super(BaseServerException, self).__init__(status_code=self.status_code, detail=detail)


class InternalServerError(BaseServerException):
    status_code = 500


class UnprocessableEntity(BaseServerException):
    status_code = 422
