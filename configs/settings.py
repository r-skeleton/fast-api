from functools import lru_cache
from typing import Optional

from kombu import Exchange, Queue, binding
from pydantic import BaseSettings


class APISettings(BaseSettings):
    description = """
    Test API Documents.

    ## Items

    Abcd **Read me**.

    """
    responses = {
        404: {'description': "Not found"}
    }

    api_metadata = {

        'title': 'Test Fast API',
        'description': description,
        'version': '0.0.1',
        'terms_of_service': 'https://www.test.fast-api.com',
        'contact': {
            'name': 'Voire',
            'url': 'https://www.linkedin.com/in/doan-manh-thang',
            'email': 'thangdoan.dev@gmail.com'
        },
        'license_info': {
            'name': 'MIT License',
            'url': 'https://www.mit.edu/~amini/LICENSE.md'
        },
        'responses': responses
    }


class CelerySettings(BaseSettings):
    queues: set = (

    )
    broker_url: str
    result_backend: Optional[str] = None
    accept_content: list = ['json']
    task_serializer: str = 'json'
    result_serializer: str = 'json'

    class Config:
        env_prefix = 'CELERY_'


@lru_cache()
def get_celery_setting() -> CelerySettings:
    return CelerySettings()


@lru_cache()
def get_setting() -> APISettings:
    return APISettings()
