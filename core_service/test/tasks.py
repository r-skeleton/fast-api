from core_service.app import init_celery_app

celery = init_celery_app()


@celery.task(name='test', queue='abcd.efg')
async def test_tasks(*args, **kwargs):
    pass
