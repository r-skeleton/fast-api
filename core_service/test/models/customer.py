from typing import Optional

from pydantic import BaseModel, Field


class Customer(BaseModel):
    name: Optional[str]
    phone_number: Optional[str]
    address: Optional[str]
    age: Optional[int] = Field(..., gt=18, description="The customer age must be greater than 18")
