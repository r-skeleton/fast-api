from typing import Optional

from pydantic import BaseModel


class Test(BaseModel):
    name: str
    description: Optional[str] = None

    class Config:
        schema_extra = {
            "example": {
                "name": "Test",
                "description": "This is a long description"
            }
        }
