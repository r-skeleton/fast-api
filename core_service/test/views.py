from fastapi import APIRouter, Body
from configs.exceptions import *
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse, HTMLResponse
from .view_model import TestModel
from .models import Test, Customer
from fastapi.security import APIKeyHeader
import logging

test_v1 = APIRouter(prefix='/v1/test', tags=['test'])

logger = logging.getLogger('api')

"""
Structures:
{
    "name": "items",
    "description": "Manage items. So _fancy_ they have their own docs.",
    "externalDocs": {
        "description": "Items external docs",
        "url": "https://fastapi.tiangolo.com/",
    }
}
"""
tags_metadata = {
    'name': 'test',
    'description': 'Operations with test'
}


@test_v1.get('/', name='Get all test objects', description='Get All the test objects')
async def get_test():
    data = await TestModel.test_function()
    return data


@test_v1.post('/test', response_model=Test)
async def post_test(item: Test = Body(...,
                                      example={
                                          "name": "This should display",
                                          "description": "This should display too"
                                      })):
    """
      Create an item with all the information:

      - **name**: each item must have a name
      - **description**: a long description
      - **price**: required
      - **tax**: if the item doesn't have tax, you can omit this
      - **tags**: a set of unique tag strings for this item
      \f
      :param item: User input.
      """
    data = await TestModel.test_again(item)
    return data


@test_v1.get('/longer', name='Get longer text', summary='This is displaying at the header of request', )
async def get_longer_text():
    return 'This is a very very very very very long long text.'


@test_v1.put('/put-test/{id}')
async def update_test_model(id: int):
    """
        Demo the dict to json using jsonable_encoder
    """
    test_obj = {'this': 'should work'}
    parsed_data = jsonable_encoder(test_obj)
    return JSONResponse(content=parsed_data)


@test_v1.get('/get-html-test')
async def update_test_model():
    test_html = """
    <html>
        <head>
            <title>Some text should be shown</title>
        </head>
        <body>
            <p>Hello</p>
        </body>
    </html>
    """
    return HTMLResponse(content=test_html, status_code=200)


@test_v1.put('/user/update', response_class=JSONResponse)
async def update_user_info(request_body: Customer = Body(
    ...,
    examples={
        'name_only': {
            'summary': 'Name only',
            'description': 'No need other information.',
            'value': {
                'name': 'John Doe'
            }
        },
        'full_update': {
            'summary': 'Full Update',
            'description': 'The ideal update body.',
            'value': {
                'name': 'John Doe',
                'phone_number': '+84123456789',
                'address': 'UK',
                'age': 15
            }
        }
    })):
    """
    Update Customer information.
    """
    data = await TestModel.update_user(request_body)
    try:
        return JSONResponse(content=jsonable_encoder({'message': data}))
    except Exception as ex:
        logger.error(f'core_service.test.views.update_user_info.error={ex}', ex)
        raise InternalServerError(detail='Cannot process further steps')
