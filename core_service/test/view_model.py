from configs.exceptions import InternalServerError
import logging

logger = logging.getLogger('api')


class TestModel:
    @classmethod
    async def test_function(cls):
        raise InternalServerError("Lỗi")

    @classmethod
    async def test_again(cls, data):
        logger.info('data')
        return data

    @classmethod
    async def update_user(cls, raw_info) -> str:
        logger.info(f'TestModel.update_user.raw_info:{type(raw_info)}')
        return 'Request succeeded'
