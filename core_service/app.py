import os
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter

from fastapi import FastAPI, Depends
from starlette.middleware.cors import CORSMiddleware
from core_service.test import test_v1, test_v1_tags
from configs.settings import APISettings, CelerySettings
from celery import Celery

logger = logging.getLogger(__name__)

# from flask import Flask
tags_metadata = [
    test_v1_tags,
]

CELERY_TASK_LIST = []


def init_celery_app(app: FastAPI = None):
    app = app or init_app()
    logger.info('here')
    celery = Celery(__name__, include=CELERY_TASK_LIST)
    celery.conf.update(CelerySettings().dict())

    class ContextTask(celery.Task):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.state.context:
                return self.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def init_app():
    app = FastAPI(**APISettings().api_metadata, openapi_tags=tags_metadata)
    app.state.context = 'TEST API'
    app.include_router(test_v1)
    setup_middleware(app)
    setup_logger(app)
    return app


# def flask_init_app():
#     app = Flask(__name__, instance_relative_config=True)
#     app.register_blueprint(flask_test_v1)
#
#     return app


def setup_logger(app):
    logger = logging.getLogger('api')
    file_handler = RotatingFileHandler('api.log', backupCount=1)
    handler = logging.StreamHandler()
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s : %(message)s '
        '[in %(module)s: %(pathname)s:%(lineno)d]'
    ))
    handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(module)s: %(pathname)s:%(lineno)d]'
    ))
    logger.addHandler(file_handler)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

def setup_extension():
    return


def setup_middleware(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )
