FROM python:3.9

WORKDIR /service

COPY ./requirements.txt /service/requirements.txt

RUN pip install -r requirements.txt

COPY . .

CMD gunicorn core_service.app:init_app --workers 4 --bind 0.0.0.0:5000 --reload --worker-class configs.uvicorn_worker.RestartableUvicornWorker --access-logfile access_apis.log

